<?php
    //Create personal information table
    mysql_query(
        "CREATE TABLE IF NOT EXISTS personal_information ("
        . "id int(11) not null auto_increment,"
        . "name varchar(255) not null,"
        . "address varchar(255) not null,"
        . "email varchar(255) not null,"
        . "phone int(255) not null,"
        . "primary key (id))") || die(mysql_error().'nula');
    
    //Create work experince table
    mysql_query(
        "CREATE TABLE IF NOT EXISTS work_experience ("
        . "we_id int(11) not null auto_increment,"
        . "we_period varchar(255) not null,"
        . "we_position varchar(255) not null,"
        . "we_employer varchar(255) not null,"
        . "primary key(we_id))") || die(mysql_error().'edno');
    
    //Create personal skills table
    mysql_query(
        "CREATE TABLE IF NOT EXISTS personal_skills ("
        . "ps_id int(11) not null auto_increment,"
        . "ps_language varchar(255) not null,"
        . "ps_communication_skills text,"
        . "ps_organisational_skills text,"
        . "primary key (ps_id))") || die(mysql_error().'dve');
?>