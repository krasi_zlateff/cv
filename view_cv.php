<?php include(dirname(__FILE__).'/header.php'); ?>
    <h1>CV Details</h1>
    <h2>Personal Information</h2>
<?php
    //select personal information data
    $personal_information = mysql_query("SELECT * FROM personal_information");
    
    //fetch the data
    $fetch_personal_information = mysql_fetch_assoc($personal_information);
    echo "Name: " . $fetch_personal_information['name'].'<br>';
    echo "Address: " . $fetch_personal_information['address'].'<br>';
    echo "Email: " . $fetch_personal_information['email'].'<br>';
    echo "Phone: " . $fetch_personal_information['phone'];
?>
    <h2>Work Experience</h2>
<?php
    //select work experience data
    $work_experience = mysql_query("SELECT * FROM work_experience");
    
    //fetch the data
    $fetch_work_experience = mysql_fetch_assoc($work_experience);
    echo "Period: " . $fetch_work_experience['we_period'].'<br>';
    echo "Position: " . $fetch_work_experience['we_position'].'<br>';
    echo "Employer: " . $fetch_work_experience['we_employer'];
?>
    <h2>Personal Skills</h2>
<?php
    //select personal skills data
    $personal_skills = mysql_query("SELECT * FROM personal_skills");
    
    //fetch the data
    $fetch_personal_skills = mysql_fetch_assoc($personal_skills);
    echo "Language: " . $fetch_personal_skills['ps_language'].'<br>';
    echo "Communication Skills: " . $fetch_personal_skills['ps_communication_skills'].'<br>';
    echo "Organisation Skills: " . $fetch_personal_skills['ps_organisational_skills'];
?>
<?php include(dirname(__FILE__).'/footer.php'); ?>

