<?php include(dirname(__FILE__).'/header.php'); ?>
    <h1>Edit CV Details</h1>
<?php
    //select personal information data
    $personal_information = mysql_query("SELECT * FROM personal_information");
    
    //fetch the data
    $fetch_personal_information = mysql_fetch_assoc($personal_information);
    
    //select work experience data
    $work_experience = mysql_query("SELECT * FROM work_experience");
    
    //fetch the data
    $fetch_work_experience = mysql_fetch_assoc($work_experience);
    
    //select personal skills data
    $personal_skills = mysql_query("SELECT * FROM personal_skills");
    
    //fetch the data
    $fetch_personal_skills = mysql_fetch_assoc($personal_skills);
?>
    <form class="update_cv" method="POST">
        <h2>Personal Information</h2>
        <!--edit name-->
        <div class="field">
            <label for="pi_name">Name: </label>
            <input type="text" name="pi_name" id="pi_name" 
               value="<?php echo $fetch_personal_information['name']; ?>" />
        </div>
        
        <!--edit address-->
        <div class="field">
            <label for="pi_address">Address: </label>
            <input type="text" name="pi_address" id="pi_address" 
               value="<?php echo $fetch_personal_information['address']; ?>" />
        </div>
        
        <!--edit email-->
        <div class="field">
            <label for="pi_email">Email: </label>
            <input type="text" name="=pi_email" id="pi_email" 
               value="<?php echo $fetch_personal_information['email']; ?>" />
        </div>
        
        <!--edit phone-->
        <div class="field">
            <label for="pi_phone">Phone: </label>
            <input type="text" name="pi_phone" id="pi_phone" 
               value="<?php echo $fetch_personal_information['phone']; ?>" />
        </div>
        
        <h2>Work Experience</h2>
        <!--edit period-->
        <div class="field">
            <label for="we_period">Period: </label>
            <input type="text" name="we_period" id="we_period" 
                   value="<?php echo $fetch_work_experience['we_period']; ?>" />
        </div>
        
        <!--edit position-->
        <div class="field">
            <label for="we_position">Position: </label>
            <input type="text" name="we_position" id="we_position" 
                   value="<?php echo $fetch_work_experience['we_position']; ?>"/>
        </div>
        
        <!--edit employer-->
        <div class="field">
            <label for="we_employer">Employer: </label>
            <input type="text" name="we_employer" id="we_employer" 
                   value="<?php echo $fetch_work_experience['we_employer']; ?>" />
        </div>
        
        <h2>Personal Skills</h2>
        <!--edit language-->
        <div class="field">
            <label for="ps_language">Language: </label>
            <input type="text" name="ps_language" id="ps_language" 
                   value="<?php echo $fetch_personal_skills['ps_language']; ?>" />
        </div>
        
        <!--edit communication skills-->
        <div class="field">
            <label for="ps_comm_skills">Communication Skills</label><br />
            <textarea name="ps_comm_skills" id="ps_comm_skills">
                <?php echo $fetch_personal_skills['ps_communication_skills']; ?>
            </textarea>
        </div>
        
        <!--edit organisations skills-->
        <div class="field">
            <label for="ps_organ_skills">Organisation Skills: </label><br />
            <textarea name="ps_organ_skills" id="ps_organ_skills">
                <?php echo $fetch_personal_skills['ps_organisational_skills']; ?>
            </textarea>
        </div>
        <button type="submit" name="update_cv">Update CV</button>
    </form>
    <?php
        if (isset($_POST['update_cv'])) {
            if (empty($_POST['pi_name'])) {
                echo 'Please fill name field';
            }
            $pi_name = mysql_real_escape_string($_POST['pi_name']);
            mysql_query("UPDATE personal_information SET name='". $pi_name ."'");
        }
    ?>
<?php include(dirname(__FILE__).'/footer.php'); ?>

