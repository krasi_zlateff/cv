<?php include(dirname(__FILE__).'/header.php')?>
    <div class="cv-form">
        <form method="POST" enctype="multipart/form-data">
            <label for="upload_cv">Browse CV</label>
            <input type="file" name="upload_cv" id="upload_cv">
            <button type="submit" name="send_cv" class="send_cv">
                Upload
            </button>
        </form>
    </div>
    <?php
        if (isset($_FILES['upload_cv']) &&
            $_FILES['upload_cv']['type'] == 'text/xml') {
            $load_cv = simplexml_load_file($_FILES['upload_cv']['name']);

        //Connect to MySQL
        require(dirname(__FILE__) .'/database/tables.php');

        //load personal information
        $pi_counter = 0;
        foreach ($load_cv->personal_information as $personal_information) {
            $pi_counter++;
            mysql_query(
                "INSERT INTO personal_information ("
                . "id, name, address, email, phone)"
                . "VALUES ("
                . "$pi_counter, "
                . "'$personal_information->name',"
                . "'$personal_information->address',"
                . "'$personal_information->email',"
                . "'$personal_information->phone')") || 
                die(mysql_error().'Can not insert personal information data');
        }
        //load work experience
        $we_counter = 0;
        foreach ($load_cv->work_experience as $work_experience) {
            $we_counter++;
            mysql_query(
                "INSERT INTO work_experience ("
                . "we_id, we_period, we_position, we_employer)"
                . "VALUES ("
                . "$we_counter,"
                . "'$work_experience->period',"
                . "'$work_experience->position',"
                . "'$work_experience->employer')") ||
                die(mysql_error().'Can not insert work experience data');
        }
        //load personal skills
        $ps_counter = 0;
        foreach ($load_cv->personal_skills as $personal_skills) {
            $ps_counter++;
            mysql_query(
                "INSERT INTO personal_skills ("
                . "ps_id, ps_language, ps_communication_skills, ps_organisational_skills)"
                . "VALUES ("
                . "$ps_counter,"
                . "'$personal_skills->language',"
                . "'$personal_skills->communication_skills',"
                . "'$personal_skills->organisational_skills')");
        }
        echo "<br>Your CV has been successfully uploaded!";
    ?>
    <br>
    <?php
        } else {
            echo "<br> Please upload a xml cv format. Thank you :)";
        }
    ?>
<?php include(dirname(__FILE__).'/footer.php')?>
